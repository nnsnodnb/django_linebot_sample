from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt
from linebot import LineBotApi, WebhookParser
from linebot.exceptions import InvalidSignatureError, LineBotApiError
from linebot.models import MessageEvent, TextMessage, TextSendMessage


line_bot_api = LineBotApi('YOURLINE_CHANNEL_ACCESS_TOKEN')
parser = WebhookParser('YOUR_LINE_CHANNEL_SECRET')


@csrf_exempt
def webhook(request):
    if request.method != 'POST':
        return HttpResponse('ん？なんやようか？', status=405)

    signature = request.META['HTTP_X_LINE_SIGNATURE']
    body = request.body.decode('utf-8')
    try:
        events = parser.parse(body, signature)
    except InvalidSignatureError:
        return HttpResponseForbidden()
    except LineBotApiError:
        return HttpResponseBadRequest()

    for event in events:
        if not isinstance(event, MessageEvent):
            continue
        if not isinstance(event.message, TextMessage):
            continue

        # テキストのみ返信
        text_send_message = TextSendMessage(text=event.message.text)
        line_bot_api.reply_message(
            event.reply_token,
            text_send_message
        )

    return HttpResponse(status=200)
